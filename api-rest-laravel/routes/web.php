<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Carga de clases
use \App\Http\Middleware\ApiAuthMiddleware;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pruebas/{nombre?}', function($nombre = null) {
    $texto = '<h2>Texto desde una ruta</h2>';
    $texto .= 'Nombre: ' . $nombre;

    return view('pruebas', array(
        'texto' => $texto
    )) ;
});

Route::get('/animales', 'PruebasController@index');
Route::get('/testORM', 'PruebasController@testORM');

/*-----------------------------------------------------------------------------------*/

    /*
        GET: conseguir datos o recursos
        POST: guardar datos o recursos o hacer logica desde un formulario
        PUT: actualizar datos o recursos
        DELETE: eliminar datos o recursos
    */

/*Rutas de prueba

        ROUTE::get('/usuario/pruebas', 'UserController@pruebas');
        ROUTE::get('/categoria/pruebas', 'CategoryController@pruebas');
        ROUTE::get('/entrada/pruebas', 'PostController@pruebas');
*/


//CONTROLADOR USUARIOS
ROUTE::post('api/register', 'UserController@register');
ROUTE::post('api/login', 'UserController@login');
ROUTE::put('api/user/update', 'UserController@update');
ROUTE::post('api/user/upload', 'UserController@uploadImage')->middleware(\ApiAuthMiddleware::class);
ROUTE::get('api/user/avatar/{filename}', 'UserController@getImage');
ROUTE::get('api/user/profile/{id}', 'UserController@profile');

//CONTROLADOR CATEGORIAS
ROUTE::resource('api/category', 'CategoryController');

//CONTROLADOR POSTS
ROUTE::resource('api/post', 'PostController');
ROUTE::post('api/post/upload', 'PostController@upload');
ROUTE::get('api/post/image/{filename}', 'PostController@getImage');
ROUTE::get('api/post/category/{id}', 'PostController@getPostsByCategory');
ROUTE::get('api/post/user/{id}', 'PostController@getPostsByUser');
